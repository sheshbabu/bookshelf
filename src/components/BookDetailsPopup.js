import React from "react";
import closeIcon from "../icons/close.svg";
import "./BookDetailsPopup.css";

export default function BookDetailsPopup(props) {
  if (!props.isPopupVisible || props.book === null) {
    return null;
  }

  return (
    <div className="BookDetailsPopup-gutter">
      <div className="BookDetailsPopup-container">
        <div className="BookDetailsPopup-close-button">
          <img src={closeIcon} onClick={props.onPopupCloseClick} />
        </div>
        <div className="BookDetailsPopup-content">
          <img
            className="BookDetailsPopup-cover"
            src={process.env.PUBLIC_URL + props.book["cover-img"]}
          />
          <div className="BookDetailsPopup-info">
            <div className="BookDetailsPopup-title">{props.book.name}</div>
            <div className="BookDetailsPopup-author">{props.book.author}</div>
          </div>
        </div>
      </div>
    </div>
  );
}
