import React from "react";
import { Switch, Route } from "react-router-dom";
import SearchBoxContainer from "../containers/SearchBoxContainer";
import BooksListContainer from "../containers/BooksListContainer";
import "./ContentArea.css";

export default function ContentArea(props) {
  return (
    <div className="ContentArea">
      <div className="ContentArea-toolbar">
        <SearchBoxContainer />
      </div>
      <div className="ContentArea-list">
        <Switch>
          <Route exact path="/" component={BooksListContainer} />
          <Route path="/favourites" component={BooksListContainer} />
          <Route path="/wishlist" component={BooksListContainer} />
        </Switch>
      </div>
    </div>
  );
}
