import React from "react";
import { NavLink } from "react-router-dom";
import "./SideNav.css";

export default function SideNav(props) {
  return (
    <div className="SideNav">
      <NavLink
        exact
        to="/"
        className="SideNav-item"
        activeClassName="SideNav-item-selected"
      >
        All Books
      </NavLink>
      <NavLink
        exact
        to="/favourites"
        className="SideNav-item"
        activeClassName="SideNav-item-selected"
      >
        Favourites
      </NavLink>
      <NavLink
        exact
        to="/wishlist"
        className="SideNav-item"
        activeClassName="SideNav-item-selected"
      >
        Wishlist
      </NavLink>
    </div>
  );
}
