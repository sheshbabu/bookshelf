import React from "react";
import { shallow } from "enzyme";
import BookItemWishlistIcon from "../BookItemWishlistIcon";

describe("BookItemWishlistIcon", () => {
  it("should be rendered without errors", () => {
    const wrapper = shallow(<BookItemWishlistIcon />);
    expect(wrapper.exists()).toBeTruthy();
  });
  it("should render 'wishlist_added.svg' icon if isAddedToWishlist is true", () => {
    const wrapper = shallow(<BookItemWishlistIcon isAddedToWishlist />);
    expect(wrapper.find("img").props().src).toEqual("wishlist_added.svg");
  });
  it("should render 'wishlist_not_added.svg' icon if isAddedToWishlist is false", () => {
    const wrapper = shallow(<BookItemWishlistIcon isAddedToWishlist={false} />);
    expect(wrapper.find("img").props().src).toEqual("wishlist_not_added.svg");
  });
  it("should call 'onRemoveFromWishlist' fn on click if isAddedToWishlist is true", () => {
    const onRemoveFromWishlistStub = jest.fn();
    const wrapper = shallow(
      <BookItemWishlistIcon
        isAddedToWishlist
        onRemoveFromWishlist={onRemoveFromWishlistStub}
      />
    );
    wrapper.simulate("click");
    expect(onRemoveFromWishlistStub).toBeCalled();
  });
  it("should call 'onAddToWishlist' fn on click if isAddedToWishlist is false", () => {
    const onAddToWishlistStub = jest.fn();
    const wrapper = shallow(
      <BookItemWishlistIcon
        isAddedToWishlist={false}
        onAddToWishlist={onAddToWishlistStub}
      />
    );
    wrapper.simulate("click");
    expect(onAddToWishlistStub).toBeCalled();
  });
});
