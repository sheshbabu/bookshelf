import React from "react";
import favoriteAddedIcon from "../../icons/favourite_added.svg";
import favoriteNotAddedIcon from "../../icons/favourite_not_added.svg";

export default function BookItemFavouriteIcon(props) {
  const icon = props.isAddedToFavourites
    ? favoriteAddedIcon
    : favoriteNotAddedIcon;
  const onClick = props.isAddedToFavourites
    ? props.onRemoveFromFavourites
    : props.onAddToFavourites;

  return <img src={icon} onClick={onClick} />;
}
