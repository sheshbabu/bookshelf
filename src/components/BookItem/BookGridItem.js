import React from "react";
import BookItemWishlistIcon from "./BookItemWishlistIcon";
import BookItemFavouriteIcon from "./BookItemFavouriteIcon";
import "./BookGridItem.css";

export default function BookGridItem(props) {
  return (
    <div className="BookGridItem">
      <div className="BookGridItem-top-section" onClick={props.onClick}>
        <img
          className="BookGridItem-cover"
          src={process.env.PUBLIC_URL + props.book["cover-img"]}
        />
        <div className="BookGridItem-title">{props.book.name}</div>
      </div>
      <div className="BookGridItem-bottom-section">
        <BookItemFavouriteIcon {...props} />
        <BookItemWishlistIcon {...props} />
      </div>
    </div>
  );
}
