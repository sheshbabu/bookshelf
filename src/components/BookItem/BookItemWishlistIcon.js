import React from "react";
import wishlistAddedIcon from "../../icons/wishlist_added.svg";
import wishlistNotAddedIcon from "../../icons/wishlist_not_added.svg";

export default function BookItemWishlistIcon(props) {
  const icon = props.isAddedToWishlist
    ? wishlistAddedIcon
    : wishlistNotAddedIcon;
  const onClick = props.isAddedToWishlist
    ? props.onRemoveFromWishlist
    : props.onAddToWishlist;

  return <img src={icon} onClick={onClick} />;
}
