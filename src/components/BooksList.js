import React from "react";
import BookItemContainer from "../containers/BookItemContainer";

export default function BooksList(props) {
  return props.books.map(book => <BookItemContainer book={book} />);
}
