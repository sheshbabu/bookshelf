import React from "react";
import "./SearchBox.css";

export default function SearchBox(props) {
  return (
    <input
      className="SearchBox"
      type="search"
      onChange={props.onChange}
      placeholder="Search for a book title..."
    />
  );
}
