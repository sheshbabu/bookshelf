import React from "react";
import Navbar from "./Navbar";
import SideNav from "./SideNav";
import ContentArea from "./ContentArea";
import BookDetailsPopupContainer from "../containers/BookDetailsPopupContainer";
import "./App.css";

export default function App() {
  return (
    <div className="App">
      <Navbar />
      <div className="App-body">
        <SideNav />
        <ContentArea />
      </div>
      <BookDetailsPopupContainer />
    </div>
  );
}
