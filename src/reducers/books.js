import { ActionTypes } from "../actions/types";

export default function(state = [], action) {
  switch (action.type) {
    case ActionTypes.LOAD_BOOKS:
      return transformInitialData(action.payload.data.books);
    default:
      return state;
  }
}

function transformInitialData(books) {
  return Object.values(books);
}
