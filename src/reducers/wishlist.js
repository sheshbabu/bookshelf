import { ActionTypes } from "../actions/types";

export default function wishlist(state = [], action) {
  switch (action.type) {
    case ActionTypes.ADD_TO_WISHLIST:
      return [...state, action.payload.book];
    case ActionTypes.REMOVE_FROM_WISHLIST:
      return state.filter(book => book.key !== action.payload.book.key);
    default:
      return state;
  }
}
