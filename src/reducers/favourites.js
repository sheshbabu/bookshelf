import { ActionTypes } from "../actions/types";

export default function favourites(state = [], action) {
  switch (action.type) {
    case ActionTypes.ADD_TO_FAVOURITES:
      return [...state, action.payload.book];
    case ActionTypes.REMOVE_FROM_FAVOURITES:
      return state.filter(book => book.key !== action.payload.book.key);
    default:
      return state;
  }
}
