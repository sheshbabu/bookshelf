import { ActionTypes } from "../../actions/types";
import favourites from "../favourites";

const book1 = {
  key: 1,
  title: "Old Book"
};
const book2 = {
  key: 2,
  title: "New Book"
};

describe("favourites reducer", () => {
  it("should return the initial state", () => {
    const expectedOutput = [];
    const actualOutput = favourites(undefined, {});
    expect(actualOutput).toEqual(expectedOutput);
  });

  it("should handle 'ADD_TO_FAVOURITES'", () => {
    const action = {
      type: ActionTypes.ADD_TO_FAVOURITES,
      payload: { book: book2 }
    };
    const initialState = [book1];
    const expectedOutput = [book1, book2];
    const actualOutput = favourites(initialState, action);
    expect(actualOutput).toEqual(expectedOutput);
    expect(expectedOutput).not.toBe(initialState); // Testing for state mutation
  });

  it("should handle 'REMOVE_FROM_FAVOURITES'", () => {
    const action = {
      type: ActionTypes.REMOVE_FROM_FAVOURITES,
      payload: { book: book2 }
    };
    const initialState = [book1, book2];
    const expectedOutput = [book1];
    const actualOutput = favourites(initialState, action);
    expect(actualOutput).toEqual(expectedOutput);
    expect(expectedOutput).not.toBe(initialState); // Testing for state mutation
  });
});
