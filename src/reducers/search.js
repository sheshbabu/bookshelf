import { ActionTypes } from "../actions/types";

export default function search(state = "", action) {
  switch (action.type) {
    case ActionTypes.SEARCH_INPUT_CHANGE:
      return action.payload.text;
    default:
      return state;
  }
}
