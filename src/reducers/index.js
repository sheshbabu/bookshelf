import { combineReducers } from "redux";
import search from "./search";
import books from "./books";
import favourites from "./favourites";
import wishlist from "./wishlist";
import ui from "./ui";

export default combineReducers({ search, books, favourites, wishlist, ui });
