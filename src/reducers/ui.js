import { ActionTypes } from "../actions/types";

const INITIAL_STATE = {
  layout: "grid",
  isPopupVisible: false,
  bookSelectedForPopup: null
};

export default function ui(state = INITIAL_STATE, action) {
  switch (action.type) {
    case ActionTypes.SHOW_POPUP:
      return {
        ...state,
        isPopupVisible: true,
        bookSelectedForPopup: action.payload.book
      };
    case ActionTypes.HIDE_POPUP:
      return {
        ...state,
        isPopupVisible: false,
        bookSelectedForPopup: null
      };
    default:
      return state;
  }
}
