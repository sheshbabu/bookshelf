import { ActionTypes } from "./types";

export function loadBooks(data) {
  return {
    type: ActionTypes.LOAD_BOOKS,
    payload: { data }
  };
}
