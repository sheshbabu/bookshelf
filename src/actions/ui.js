import { ActionTypes } from "../actions/types";

export function showPopup(book) {
  return {
    type: ActionTypes.SHOW_POPUP,
    payload: { book }
  };
}

export function hidePopup() {
  return {
    type: ActionTypes.HIDE_POPUP
  };
}
