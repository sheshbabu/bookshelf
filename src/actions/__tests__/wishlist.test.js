import { ActionTypes } from "../../actions/types";
import { addToWishlist, removeFromWishlist } from "../wishlist";

const book = {
  key: 1,
  title: "Old Book"
};

describe("wishlist actions", () => {
  it("should create an action to add to wishlist", () => {
    const expectedOutput = {
      type: ActionTypes.ADD_TO_WISHLIST,
      payload: { book }
    };
    const actualOutput = addToWishlist(book);
    expect(actualOutput).toEqual(expectedOutput);
  });
  it("should create an action to remove from wishlist", () => {
    const expectedOutput = {
      type: ActionTypes.REMOVE_FROM_WISHLIST,
      payload: { book }
    };
    const actualOutput = removeFromWishlist(book);
    expect(actualOutput).toEqual(expectedOutput);
  });
});
