import { ActionTypes } from "../actions/types";

export function addToWishlist(book) {
  return {
    type: ActionTypes.ADD_TO_WISHLIST,
    payload: { book }
  };
}

export function removeFromWishlist(book) {
  return {
    type: ActionTypes.REMOVE_FROM_WISHLIST,
    payload: { book }
  };
}
