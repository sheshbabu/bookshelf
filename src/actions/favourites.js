import { ActionTypes } from "../actions/types";

export function addToFavourites(book) {
  return {
    type: ActionTypes.ADD_TO_FAVOURITES,
    payload: { book }
  };
}

export function removeFromFavourites(book) {
  return {
    type: ActionTypes.REMOVE_FROM_FAVOURITES,
    payload: { book }
  };
}
