/*
  Contains all the actions in the app
  This gives developers an overview of the all the actions available
    and also prevents us from adding duplicate actions
*/

export const ActionTypes = {
  // search
  SEARCH_INPUT_CHANGE: "SEARCH_INPUT_CHANGE",

  // books
  LOAD_BOOKS: "LOAD_BOOKS",

  // favorites
  ADD_TO_FAVOURITES: "ADD_TO_FAVOURITES",
  REMOVE_FROM_FAVOURITES: "REMOVE_FROM_FAVOURITES",

  // wishlist
  ADD_TO_WISHLIST: "ADD_TO_WISHLIST",
  REMOVE_FROM_WISHLIST: "REMOVE_FROM_WISHLIST",

  // ui
  SHOW_POPUP: "SHOW_POPUP",
  HIDE_POPUP: "HIDE_POPUP"
};
