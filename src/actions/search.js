import { ActionTypes } from "../actions/types";

export function changeSearchText(text) {
  return {
    type: ActionTypes.SEARCH_INPUT_CHANGE,
    payload: { text }
  };
}
