import { connect } from "react-redux";
import BooksList from "../components/BooksList";

export default connect(mapStateToProps)(BooksList);

function mapStateToProps(state, ownProps) {
  const layout = state.ui.layout;
  const searchTerm = state.search;
  let books = state.books;

  if (ownProps.location.pathname === "/wishlist") {
    books = state.wishlist;
  } else if (ownProps.location.pathname === "/favourites") {
    books = state.favourites;
  }

  books = filterBooksMatchingSearchTerm(books, searchTerm);

  return {
    layout,
    books
  };
}

function filterBooksMatchingSearchTerm(books, searchTerm) {
  return books.filter(book => book.name.toLowerCase().includes(searchTerm));
}
