import { connect } from "react-redux";
import { changeSearchText } from "../actions/search";
import SearchBox from "../components/SearchBox";

export default connect(mapStateToProps, mapDispatchToProps)(SearchBox);

function mapStateToProps(state) {
  const text = state.search;
  return { text };
}

function mapDispatchToProps(dispatch) {
  const onChange = event => dispatch(changeSearchText(event.target.value));
  return { onChange };
}
