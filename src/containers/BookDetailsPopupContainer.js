import { connect } from "react-redux";
import { hidePopup } from "../actions/ui";
import BookDetailsPopup from "../components/BookDetailsPopup";

export default connect(mapStateToProps, mapDispatchToProps)(BookDetailsPopup);

function mapStateToProps(state) {
  const isPopupVisible = state.ui.isPopupVisible;
  const book = state.ui.bookSelectedForPopup;
  return { isPopupVisible, book };
}

function mapDispatchToProps(dispatch) {
  const onPopupCloseClick = () => dispatch(hidePopup());
  return { onPopupCloseClick };
}
