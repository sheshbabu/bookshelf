import { connect } from "react-redux";
import { addToFavourites, removeFromFavourites } from "../actions/favourites";
import { addToWishlist, removeFromWishlist } from "../actions/wishlist";
import { showPopup } from "../actions/ui";
import BookGridItem from "../components/BookItem/BookGridItem";

export default connect(mapStateToProps, mapDispatchToProps)(BookGridItem);

function mapStateToProps(state, ownProps) {
  const { book } = ownProps;
  const isAddedToWishlist = Boolean(
    state.wishlist.find(item => item.key === book.key)
  );
  const isAddedToFavourites = Boolean(
    state.favourites.find(item => item.key === book.key)
  );

  return {
    isAddedToWishlist,
    isAddedToFavourites
  };
}

function mapDispatchToProps(dispatch, ownProps) {
  const { book } = ownProps;
  const onClick = () => dispatch(showPopup(book));
  const onAddToWishlist = () => dispatch(addToWishlist(book));
  const onRemoveFromWishlist = () => dispatch(removeFromWishlist(book));
  const onAddToFavourites = () => dispatch(addToFavourites(book));
  const onRemoveFromFavourites = () => dispatch(removeFromFavourites(book));

  return {
    onClick,
    onAddToWishlist,
    onRemoveFromWishlist,
    onAddToFavourites,
    onRemoveFromFavourites
  };
}
